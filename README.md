## Getting Started

### Create Global Environment
```bash
opam update
opam init -y
eval $(opam env)
```

### Create Dev Environment

```bash
opam switch create . 5.1.1 -y --deps-only
eval $(opam env)
```

touch `.gitignore`

`.gitignore`:
```.gitignore
_build
_opam

*.swp
```

### Install Dependencies

```bash
opam install reason
refmt --version # to validate

opam install dune
```

### Create New Project

```bash
dune init (proj|exec|lib|test) NAME [PATH]
```

### Run and Build Project

```bash
dune exec NAME
# or
dune build
```
